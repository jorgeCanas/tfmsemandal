from celery import shared_task, current_task
from django.core.management import call_command
from cStringIO import StringIO

@shared_task
def create_context(delete, n_notices, percentage, limit, n_notices_test):
    """
    create the context and the notices test set
    """
    stdout = StringIO()
    process_status = call_command('limpiar_bd', delete=delete, )

    current_task.update_state(state='PROGRESS',
                              meta={'process_status': process_status})

@shared_task
def test_task(n_notices_test):
    print 'test_task ' + str(n_notices_test)
    """
    create the context and the notices test set
    """
    stdout = StringIO()
    call_command('get_random_notices_with_categories', number_of_notices=n_notices_test, stdout=stdout)
    stdout.seek(0)
    process_status = stdout.getvalue()

    current_task.update_state(state='PROGRESS',
                              meta={'process_status': process_status})

"""
 - tasks.py (place it in the same place with settings.py, testcele/tasks.py):

from celery import task, current_task
from celery.result import AsyncResult
from time import sleep
from testcele.cele import models


NUM_OBJ_TO_CREATE = 1000

# when this task is called, it will create 1000 objects in the database
@task()
def create_models():
 for i in range(1, NUM_OBJ_TO_CREATE+1):
  fn = 'Fn %s' % i
  ln = 'Ln %s' % i
  my_model = models.MyModel(fn=fn, ln=ln)
  my_model.save()

  process_percent = int(100 * float(i) / float(NUM_OBJ_TO_CREATE))

  sleep(0.1)
  current_task.update_state(state='PROGRESS',
    meta={'process_percent': process_percent})

"""