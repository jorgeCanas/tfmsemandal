#!/usr/bin/python
# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand, CommandError
from pueblos.models import PueblosNoticias200, PueblosNc, PueblosCategoriasSemandal, PueblosNoticiasPalabras
from pueblos.common.util.utilities import clean_text, levenshtein
from collections import defaultdict, OrderedDict
import re
import time
import math
import os

SITE_ROOT = os.path.abspath(os.path.dirname('__file__'))

class Command(BaseCommand):
    help = 'Genera la lista de palabras asociado a cada noticia'

    def handle(self, *args, **options):
        # Crea diccionario con categorías y sus palabras asociadas
        palabras_categoria = defaultdict(dict)
        # lista_categorias = PueblosCategoriasSemandal.objects.all()

        lista_cat = []
        """
        for categoria in lista_categorias:
            etiqueta = 'ETIQUETA_' + re.sub(ur'[ ]', '_', categoria.dscategoria.lower())
            lista_cat.append(etiqueta)
        lista_cat.sort()
        print lista_cat
        """
        total_palabras_categoria = {}
        """
        for categoria in lista_cat:
            total_palabras_categoria[categoria] = 0.0
        """
        #palabras = PueblosNoticiasPalabras.objects.all()[:100]
        palabras = PueblosNoticiasPalabras.objects.all() # todos
            
        # palabras = PueblosNoticiasPalabras.objects.filter(palabra='ayuntamiento')
        for palabra in palabras:
            if len(palabra.palabra) > 3 and len(palabra.palabra) < 30:
                categorias = palabra.get_categorias_string()
                my_word = palabra.palabra
                #Si palabra no esta en el diccionario se generan todas las categorias asociadas a la palabra
                """
                if my_word not in palabras_categoria:
                    for categoria in lista_cat:
                        palabras_categoria[my_word][categoria] = 0.0
                """
                for categoria in categorias:
                    etiqueta = 'ETIQUETA_' + re.sub(ur'[ ]', '_', categoria.lower())
                    if etiqueta not in lista_cat:
                        lista_cat.append(etiqueta)
                        
                    if my_word not in palabras_categoria:
                        palabras_categoria[my_word][etiqueta] = 1.0
                    elif etiqueta not in palabras_categoria[my_word]:
                        palabras_categoria[my_word][etiqueta] = 1.0
                    else:
                        palabras_categoria[my_word][etiqueta] += 1.0
                    
                    if etiqueta not in total_palabras_categoria:
                        total_palabras_categoria[etiqueta] = 1.0
                    else:
                        total_palabras_categoria[etiqueta] += 1.0
        lista_cat.sort()
        # print lista_cat
        # print total_palabras_categoria
        print 'Terminado diccionario'
        # Genera los índices para cada palabra según la fórmula: TF*IDF
        total_palabras_categoria = OrderedDict(sorted(total_palabras_categoria.items(), key=lambda x: x[1], reverse=True))
        # print total_palabras_categoria
        total_cat = len(lista_cat)
        max = 4
        imprimible = 0
        show_once = True
        for words, category in palabras_categoria.items():
            # print words
            imprimible = 0
            TF = 0
            IDF = 0
            cat_aparece = 0.0
            for cat, value in category.items():
                if value > 0.0:
                    cat_aparece += 1.0
            media_tf_idf = 0.0
            for cat, value in category.items():
                if total_palabras_categoria[cat] > 0 and value > 1:
                    # print str(value) + ' total ' + str(total_palabras_categoria[cat])
                    TF = value / total_palabras_categoria[cat]
                    IDF = total_cat / cat_aparece
                    """
                    if imprimible < max:
                        print 'valor ' + str(value)
                        print 'TF ' + str(TF)
                        print 'IDF antes ' + str(IDF)
                        IDF = math.log(IDF, 10)
                        print 'IDF despues ' + str(IDF)
                        TF_IDF = TF * IDF
                        print 'TF*IDF ' + str(TF_IDF)
                        imprimible += 1
                    """
                    IDF = math.log(IDF, 10)
                    TF_IDF = TF * IDF
                    media_tf_idf += TF_IDF
                    """
                    if TF_IDF > 0.01:
                        print words
                        print cat
                        print 'TF*IDF ' + str(TF_IDF)
                    """
        print 'Terminado TF-IDF'

        #Preparar elementos para generar archivo
        listaCatPal = []

        #filename = 'contexto' + str(time.time()) + '.cxt'
        filename = SITE_ROOT + '/files/' + 'contexto.csv'
        writer = open(filename, 'w')

        for category, value in palabras_categoria[palabras[0].palabra].items():
            listaCatPal.append(category)

        for word in palabras_categoria.items():
            if word[0] not in listaCatPal:
                listaCatPal.append(word[0])
            # print word[0]
        listaCatPal.sort()
        # print listaCatPal
        #cabecera
        noticias = PueblosNoticias200.objects.all()

        #Formato
        """
        primera línea: Lista de atributos, en este caso categorías y palabras
        resto de líneas - objetos: Primer elemento id de la noticia, resto palabras con buen índice en la noticia
        """
        line = ''
        for w in listaCatPal:
            w = re.sub(ur'[ ]', '_', w)
            line += w+';'
        writer.write(line[:-1].encode('UTF-8')+'\n')

        for noticia in noticias:
            line = str(noticia.id) + ';'
            #Categorias asociadas a la noticia
            categorias = noticia.get_categorias_string()
            for categoria in categorias:
                categoria = re.sub(ur'[ ]', '_', categoria)
                line += categoria + ';'
            
            palNot = PueblosNoticiasPalabras.objects.filter(noticia_200=noticia.id)
            lista = []
            for pal in palNot:
                lista.append(pal.palabra)
            lista.sort()
            # print lista
            
            elem = lista.pop(0)
            
            for pal in listaCatPal:
                if elem == pal:
                    # print elem
                    if elem not in categorias: # Es una categoria
                        for cat in categorias:
                            # print str(palabras_categoria[pal][cat])
                            #if palabras_categoria[elem][cat] > 0.33:
                            if palabras_categoria[elem][cat] > 0.66:
                                line += elem + ';'
                                break
                    if lista: #Si la lista tiene elementos
                        elem = lista.pop(0)
            #print line
            writer.write(line[:-1].encode('UTF-8')+'\n')

        writer.close()
        print 'Terminado'