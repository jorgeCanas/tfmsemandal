#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.db import connection
from django.core.management.base import BaseCommand, CommandError
from pueblos.models import PueblosNoticias200, PueblosNc
from pueblos.common.util.utilities import esta, clean_text, clean_accents
from collections import OrderedDict, defaultdict
import string
import re
import time

class Command(BaseCommand):
    help = 'Genera la lista de palabras y categorías asociado a las noticias de test'

    def handle(self, *args, **options):
        notices = PueblosNoticias200.objects.all()
        # File containing each notice "clean" words, id and category
        words_filename = 'files/words_category_notices'+ str(time.strftime('%d%m%Y')) +'.csv'
        # File containing each notice text, id and category
        notice_filename = 'files/notices'+ str(time.strftime('%d%m%Y')) +'.csv'
        notice_writer = open(notice_filename, 'w')
        word_writer = open(words_filename, 'w')
        for notice in notices:
            categories = notice.get_categorias_string()
            label_line = ''
            for category in categories:
                label = 'ETIQUETA_' + re.sub(ur'[ ]', '_', category.lower())
                label = clean_accents(label)
                label_line += label + ','
            ############# Full notice save ###########################
            notice_body = re.sub(ur'[;]', ' ', notice.dscuerpo)
            notice_body = re.sub(ur'[ ]+', ' ', notice_body)
            notice_text_line = str(notice.id) + ';' + label_line[:-1] + ';' + notice_body
            notice_text_line = notice_text_line.encode('utf-8')
            notice_writer.write(notice_text_line + '\n')
            ############# Notice words save ###########################
            notice_words = re.sub(ur'[^a-zA-ZÁÉÍÓÚÜáéíóúüñÑ]', ' ', notice.dscuerpo)
            notice_words = clean_accents(notice_words)
            notice_words = re.sub(ur'[ ]+', ' ', notice_words)

            notice_words = notice_words.split(' ')
            notice_words[:] = [x.strip().lower() for x in notice_words if not clean_text(x)]
            
            line = ''
            for w in notice_words:
                line += w + ','
            line = str(notice.id) + ';' + label_line[:-1] + ';' + line[:-1]
            line = line.encode('utf-8')
            word_writer.write(line +'\n')
        word_writer.close()
        notice_writer.close()
        print 'Finished'