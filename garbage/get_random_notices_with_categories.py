#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.db import connection
from django.core.management.base import BaseCommand
from pueblos.models import PueblosNoticias
from pueblos.common.util.utilities import clean_text, clean_accents, get_list_words
import re
import time


class Command(BaseCommand):
    """Use example:
             python manage.py get_random_notices_with_categories 50 --outnotices files/notices.csv
             --outwords files/words.csv"""
    help = 'Genera la lista de palabras y categorias asociadas a una noticia aleatoria categorizada'

    def add_arguments(self, parser):
        parser.add_argument('number_of_notices', nargs='?', type=int, default=0, help='Numero de noticias a coger')
        parser.add_argument('--outnotices', dest='outnotices', type=str, nargs='?', default='',
                            help='Fichero de salida noticias')
        parser.add_argument('--outwords', dest='outwords', type=str, nargs='?', default='',
                            help='Fichero de salida palabras noticia')

    def handle(self, *args, **options):
        if options['number_of_notices']:
            # File containing each notice "clean" words, id and category
            if options['outwords']:
                words_filename = options['outwords']
            else:
                words_filename = 'files/rand_words_category_notices' + str(time.strftime('%d%m%Y')) + '.csv'
            # File containing each notice text, id and category
            if options['outnotices']:
                notice_filename = options['outnotices']
            else:
                notice_filename = 'files/rand_notices' + str(time.strftime('%d%m%Y')) + '.csv'
            with connection.cursor() as cursor:
                cursor = connection.cursor()
                limit = options['number_of_notices']
                query = """
                    SELECT DISTINCT noticia.id, noticia.dscuerpo
                    FROM pueblos_noticias noticia
                    LEFT JOIN pueblos_noticias_200 noticia200
                        ON noticia200.noticia_id = noticia.id
                    INNER JOIN pueblos_nc nc
                        ON noticia.id = nc.noticia_id
                            AND nc.categoria_id != 53
                            AND length(noticia.dscuerpo) > 20
                    WHERE noticia200.noticia_id IS NULL
                    AND noticia.dscuerpo REGEXP '^[A-Za-z0-9ÁÉÍÓÚÜáéíóúüñÑ \t\n?=_+-.,!@#\€\¡\¿$%%^&*()\º\ª;:\\\/|<>"''-~[]+$'
                    ORDER BY RAND()
                    LIMIT %s
                    """
                cursor.execute(query, (int(limit),))
                count = cursor.rowcount
                notice_writer = open(notice_filename, 'w')
                word_writer = open(words_filename, 'w')
                for notice in cursor:
                    categorized_notice = PueblosNoticias.objects.get(id=notice[0])
                    categories = categorized_notice.get_categorias_string()
                    label_line = ''
                    for category in categories:
                        label = 'ETIQUETA_' + re.sub(ur'[ ]', '_', category.lower())
                        label = clean_accents(label)
                        label_line += label + ','
                    ############# Full notice save ###########################
                    notice_body = re.sub(ur'[;]', ' ', notice[1])
                    notice_body = re.sub(ur'[ ]+', ' ', notice_body)
                    notice_text_line = str(notice[0]) + ';' + label_line[:-1] + ';' + notice_body
                    notice_text_line = notice_text_line.encode('utf-8')
                    notice_writer.write(notice_text_line + '\n')
                    ############# Notice words save ###########################
                    """
                    notice_words = re.sub(ur'[^a-zA-ZÁÉÍÓÚÜáéíóúüñÑ]', ' ', notice[1])
                    notice_words = clean_accents(notice_words)
                    notice_words = re.sub(ur'[ ]+', ' ', notice_words)

                    notice_words = notice_words.split(' ')
                    notice_words[:] = [x.strip().lower() for x in notice_words if not clean_text(x)]
                    """
                    notice_words = get_list_words(notice[1])
                    line = ''
                    for w in notice_words:
                        # line += w + ','
                        line += w + ';'
                    # line = str(notice[0]) + ';' + label_line[:-1] + ';' + line[:-1]
                    line = str(notice[0]) + ';' + line[:-1]
                    line = line.encode('utf-8')
                    word_writer.write(line + '\n')
                word_writer.close()
                notice_writer.close()
                # return self.stdout.write('Terminado con ' + str(count) + ' noticias devueltas')
                return self.stdout.write('Finished')
        else:
            self.stderr.write('Number of notices not set, please provide this parameters')
