#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.db import connection
from django.core.management.base import BaseCommand, CommandError
from pueblos.models import PueblosNoticias, PueblosNc
from pueblos.common.util.utilities import esta, clean_text, clean_accents
from collections import OrderedDict, defaultdict
import string
import re
import time

class Command(BaseCommand):
    help = 'Genera la lista de palabras asociado a una noticia aleatoria'

    def add_arguments(self, parser):
        # Named (optional) arguments
        parser.add_argument('--identified',
            action='store_true',
            dest='identified',
            default=False,
            help='Coge noticias con categoria')
        parser.add_argument('number_of_notices',
            default=False,
            help='Numero de noticias a coger')

    def handle(self, *args, **options):
        cursor = connection.cursor()
        limit = options['number_of_notices']
        if not limit.isdigit():
            print "Inserte un número correcto"
            return
        if options['identified']:
            query = """
                SELECT DISTINCT noticia.id, noticia.dscuerpo
                FROM pueblos_noticias noticia
                LEFT JOIN pueblos_noticias_200 noticia200
                    ON noticia200.noticia_id = noticia.id
                INNER JOIN pueblos_nc nc
                    ON noticia.id = nc.noticia_id 
                        AND nc.categoria_id != 53 
                        AND length(noticia.dscuerpo) > 20
                WHERE noticia200.noticia_id IS NULL
                ORDER BY RAND()
                LIMIT %s
                """
            cursor.execute(query, [int(limit)])
        #noticias = cursor.fetchone()
        #filename = 'files/palabras_noticia.csv'
        filename = 'files/palabras_noticia'+ str(time.strftime('%d%m%Y')) +'.csv'
        notice_filename = 'files/noticia'+ str(time.strftime('%d%m%Y')) +'.csv'
        notice_writer = open(notice_filename, 'w')
        writer = open(filename, 'w')
        for noticia in cursor:
            # print 'Noticia sin tratar----> '+noticia[1]
            # texto_noticia = noticia[1].encode('utf-8')
            # print '--->Noticia sin tratar utf-8--> ' + texto_noticia
            
            #Save the notice so it can be checked
            notice = noticia[1].encode('utf-8')
            notice_writer.write(str(noticia[0]) + ';' + notice + '\n')

            lista_palabras = re.sub(ur'[^a-zA-ZÁÉÍÓÚÜáéíóúüñÑ]', ' ', noticia[1])
            lista_palabras = clean_accents(lista_palabras)
            lista_palabras = re.sub(ur'[ ]+', ' ', lista_palabras)

            # print 'Noticia pre-tratada----> '+lista_palabras

            lista_palabras = lista_palabras.split(' ')
            lista_palabras[:] = [x.strip().lower() for x in lista_palabras if not clean_text(x)]
            # lista_palabras.sort()
            
            # print 'Noticia limpia '
            # print lista_palabras
            # generar lista de palabras de la noticia, no importa si se repiten
            line = ''
            for w in lista_palabras:
                line += w + ';'
            line = line.encode('utf-8')
            writer.write(line[:-1]+'\n')
            """
            hashmap = {}
            w = lista_palabras.pop(0)
            plural = {}

            hashmap[w] = w
            for w in lista_palabras:
                #if len(w) > 3:
                if w[-1:] == 's':
                    plural[w] = w
                else:
                    if not esta(w, hashmap):
                        hashmap[w] = w

            for key, w in plural.items(): # se busca si existen palabras parecidas en singular, sino se almacena
                # print '-------------------------------------Plurales--------------------'
                encontrado = False
                word = w
                if w[-3:] == 'ces' and len(w) > 5:
                    w = w[:-3] + 'z'
                if w[-2:] == 'es' and len(w) > 5:
                    w = w[:-1] # se quita la s final
                    if not esta(w[:-1], hashmap):
                        encontrado = True
                        hashmap[word] = word
                if not encontrado and not esta(w, hashmap):
                    hashmap[word] = word

            line = ''
            for key, value in hashmap.items():
                line += value+';'
            line = line.encode('utf-8')
            writer.write(line[:-1]+'\n')
            """
        writer.close()
        notice_writer.close()
        print 'Terminado'