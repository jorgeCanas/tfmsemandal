#!/usr/bin/python
# -*- coding: utf-8 -*-

import re

from django.core.management.base import BaseCommand
from pueblos.SemanticMotor.ConfusionMatrix import ConfusionMatrix
from pueblos.common.util.utilities import clean_accents, get_list_words
from pueblos.SemanticMotor.SemanticMotor import SemanticMotor
from pueblos.models import PueblosNoticias, PueblosNoticiasTest, PueblosNoticiasTestCategorias


class Command(BaseCommand):
    """Use example:
     python manage.py test_context_set_matrix --test_file prueba_words_category.csv --output_file confusion_matrix"""
    help = 'Realiza el test del contexto creado y genera matrix de confusion'

    def add_arguments(self, parser):
        parser.add_argument('--output_file', dest='output_file', nargs='?', default='',
                            help='Nombre del fichero donde se guardara la matriz de confusion')
        parser.add_argument('--min_confidence', dest='min_confidence', nargs='?', default='0.8',
                            help='Confianza mínimia que tiene que cumplir la regla para ser sugerida')

    def handle(self, *args, **options):
        n_stored = 0
        n_suggested = 0
        output = ''
        output_file = options['output_file']
        savefile = 'files/confusion_matrix.csv'

        if output_file:
            if 'files_batch/' in output_file:
                savefile = output_file
            elif 'files/' not in output_file:
                savefile = 'files/' + output_file

            if '.csv' not in savefile:
                savefile = output_file + '.csv'
        results_file = savefile[:-4]
        sm = SemanticMotor()
        # sm.save_semantic_motor('files/semantic_motor.txt')
        test_notices = PueblosNoticiasTest.objects.all()
        notice_results = {}
        labels = dict()
        with open(results_file + '-rules-track.txt', 'w') as fout:
            fout.write('')
        for test_notice in test_notices:
            notice_words = get_list_words(test_notice.dscuerpo)
            results = sm.get_conclusion_names(notice_words)
            suggested_categories = []
            rule_output = str(test_notice.id) + '\n'
            for result in results:  # Testing purpose, estimate rule support
                category = result.category
                # if result.rule.get_confidence() >= 0.6:
                rule_output += category + ';'
                rule_output += result.rule.str_csv() + '\n'
            with open(results_file + '-rules-track.txt', 'a') as fout:
                rule_output += '--------------- Next Notice --------------------\n'
                fout.write(rule_output.encode('utf-8'))
            for result in results:  # Get at least one category?
                category = result.category

                """
                if category not in suggested_categories and result.rule.get_confidence() >= 0.6:
                """
                if category not in suggested_categories and (
                                # result.rule.get_confidence() >= 0.6 or result.rule.get_support() > 10):
                                result.rule.get_confidence() >= 0.6):
                    suggested_categories.append(category)
                    # labels[category] = category
                    labels.update({category: category})
            for suggested_category in suggested_categories:
                notice_suggested_category = PueblosNoticiasTestCategorias(
                    id=None, noticia_test=test_notice, categorias_sugeridas=suggested_category)
                notice_suggested_category.save()

            notice = PueblosNoticias.objects.get(id=test_notice.noticia_id)
            categories = notice.get_categorias_string()
            clean_categories = []
            for category in categories:
                category = 'ETIQUETA_' + re.sub(ur'[ ]', '_', category.lower())
                category = clean_accents(category)
                clean_categories.append(category)
                # labels[category] = category
                labels.update({category: category})
            notice_results[test_notice.noticia_id] = [clean_categories, suggested_categories]
        cm_labels = labels.keys()
        cm = ConfusionMatrix(labels=cm_labels)
        total_elements = 0.0
        correct_result = 0.0
        for notice, list_categories in notice_results.items():
            stored_categories, suggested_categories = list_categories
            n_stored += len(stored_categories)
            n_suggested += len(suggested_categories)
            if sm.check_correct(stored_categories, suggested_categories):
                result_text = ' Contiene las etiquetas'
                correct_result += 1.0
            else:
                result_text = ' No contiene las etiquetas'
            total_elements += 1.0
            output += 'id ' + str(notice) + ';stored ' + str(stored_categories) + ';suggested' \
                      + str(suggested_categories) + result_text + '\n'
            cm.set_values(previous_categories=stored_categories, suggested_categories=suggested_categories)

        output += 'Total number of stored categories ' + str(n_stored) + '\n'
        output += 'Total number of suggested categories ' + str(n_suggested) + '\n'
        output += 'Total number of element check ' + str(total_elements) + '\n'
        output += 'Total number of correct element ' + str(correct_result) + '\n'
        output += 'Correct percentage ' + str(correct_result / total_elements * 100.0) + '%\n'
        # print cm
        message = cm.save_confusion_matrix(savefile)
        if message is 'Saved':
            # Quit the .csv extension
            with open(results_file + '-track.txt', 'w') as fout:
                fout.write(output.encode('utf-8'))
            with open(results_file + '-results.txt', 'w') as fout:
                fout.write(cm.get_results())
            self.stdout.write('Finished')
        else:
            self.stdout.write('Error opening the output file')
